module.exports = class search_condition {
    constructor(title, comment, deadline_from, deadline_to, priority, finish, sort_key, asc) {
        this.title = title;
        this.comment = comment;
        this.deadline_from = Number(deadline_from);
        this.deadline_to = Number(deadline_to);
        this.priority = Number(priority);
        this.finish = Number(finish);
        this.sort_key = sort_key;
        this.asc = asc;
    }

    static fromHash(hash) {
        return !!!hash ?
            new this() :
            new this(hash.title, hash.comment, hash.deadline_from, hash.deadline_to, hash.priority, hash.finish, hash.sort_key, hash.asc);
    }
}