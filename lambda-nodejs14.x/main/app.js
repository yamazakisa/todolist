var bizlogic = require("./bizlogic");

exports.lambdaHandler = async (event, context) => {
    var response = {
        "statusCode": 200,
        "headers": {
          "Content-Type": "application/json",
          "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
          "Access-Control-Allow-Origin": "*" ,
          "Access-Control-Allow-Methods": "GET,POST,OPTIONS"
        },
    };

    // /todolist/xxxを取得
    var paths = event.path.split("/");
    var path = paths.slice(-1)[0];

    switch (path) {
        case "create":
            var body = JSON.parse(event.body);
            result = await bizlogic.create(body);
            response["body"] = JSON.stringify(result);
            break;
        case "update":
            var body = JSON.parse(event.body);
            result = await bizlogic.update(body);
            response["body"] = JSON.stringify(result);
            break;
        case "delete":
            var body = JSON.parse(event.body);
            await bizlogic.delete(body);
            break;
        case "search":
            result = await bizlogic.search(event.queryStringParameters);
            response["body"] = JSON.stringify(result);
            break;
        default:
            throw "not implement";
    };

    return response;
};