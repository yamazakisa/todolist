module.exports = class todo {
    constructor(todo_id, title, comment, create_date, deadline, priority, finish_flag) {
        this.todo_id = todo_id;
        this.title = title;
        this.comment = comment;
        this.create_date = create_date;
        this.deadline = deadline;
        this.priority = priority;
        this.finish_flag = finish_flag;
    }

    static fromHash(hash) {
        return new this(hash.todo_id, hash.title, hash.comment, hash.create_date, hash.deadline, hash.priority, hash.finish_flag);
    }
}