var AWS = require("aws-sdk");
var uuid = require("uuid");
var todo = require("./todo");
var serach_condition = require("./search_condition");

var docClient = new AWS.DynamoDB.DocumentClient();
var tableName = "todo";

exports.create = async function (body) {
    var item = todo.fromHash(body);

    item.todo_id = uuid.v4();
    item.create_date = Date.now();
    item.deadline = item.deadline;
    item.delete_flag = false;
    item.finish_flag = !!item.finish_flag;

    var params = {
        TableName: tableName,
        Item: item,
    };

    await docClient.put(params, function (err, data) {
        if (err) {
            console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("Added item:", JSON.stringify(data, null, 2));
        }
    }).promise();

    return item;
}

exports.update = async function (body) {
    var item = todo.fromHash(body);

    var params = {
        TableName: tableName,
        Key: {
            "todo_id": item.todo_id,
            "create_date": item.create_date,
        },
        UpdateExpression: "set #t=:t, #c=:c, #d=:d, #p=:p, #f=:f",
        ExpressionAttributeNames: {
            "#t": "title",
            "#c": "comment",
            "#d": "deadline",
            "#p": "priority",
            "#f": "finish_flag",
        },
        // undefinedだとエラーになるのでnullにする
        ExpressionAttributeValues: {
            ":t": item.title ?? null,
            ":c": item.comment ?? null,
            ":d": item.deadline ?? null,
            ":p": item.priority ?? null,
            ":f": item.finish_flag ?? false,
        },
    };

    await docClient.update(params, function (err, data) {
        if (err) {
            console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
        }
    }).promise();

    return item;
}

exports.delete = async function (body) {
    var item = todo.fromHash(body);

    var params = {
        TableName: tableName,
        Key: {
            "todo_id": item.todo_id,
            "create_date": item.create_date,
        },
        UpdateExpression: "set #d=:d",
        ExpressionAttributeNames: {
            "#d": "delete_flag",
        },
        ExpressionAttributeValues: {
            ":d": true,
        },
    };

    await docClient.update(params, function (err, data) {
        if (err) {
            console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
        }
    }).promise();
}

exports.search = async function (body) {
    var contidion = serach_condition.fromHash(body);

    var params = {
        TableName: tableName,
        Limit: 100,
        ProjectionExpression: "#to, #cr, #ti, #co, #dl, #pr, #fi",
        FilterExpression: "#df = :false",
        ExpressionAttributeNames: {
            "#to": "todo_id",
            "#cr": "create_date",
            "#ti": "title",
            "#co": "comment",
            "#dl": "deadline",
            "#pr": "priority",
            "#fi": "finish_flag",
            "#df": "delete_flag",
        },
        ExpressionAttributeValues: {
            ":false": false,
        }
    };

    if (!!contidion.title) {
        params.FilterExpression += " and contains(#ti, :ti)";
        params.ExpressionAttributeValues[":ti"] = contidion.title;
    }

    if (!!contidion.comment) {
        params.FilterExpression += " and contains(#co, :co)";
        params.ExpressionAttributeValues[":co"] = contidion.comment;
    }

    if (!!contidion.deadline_from) {
        params.FilterExpression += " and #dl >= :dlf";
        params.ExpressionAttributeValues[":dlf"] = contidion.deadline_from;
    }

    if (!!contidion.deadline_to) {
        params.FilterExpression += " and #dl <= :dlt";
        params.ExpressionAttributeValues[":dlt"] = contidion.deadline_to;
    }

    if (!!contidion.priority) {
        params.FilterExpression += " and #pr = :pr";
        params.ExpressionAttributeValues[":pr"] = contidion.priority;
    }

    if (!!contidion.finish) {
        params.FilterExpression += " and #fi = :fi";
        // 1＝未完了、2＝完了
        params.ExpressionAttributeValues[":fi"] = contidion.finish == 2;
    }

    var todos = [];
    await docClient.scan(params, onScan).promise();
    todos.sort((a, b)=> {
        if(a.create_date < b.create_date) return 1;
        if(a.create_date > b.create_date) return -1;
        return 0;
    });
    return todos;

    function onScan(err, data) {
        if (err) {
            console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            todos = todos.concat(data.Items);

            // continue scanning if we have more movies, because
            // scan can retrieve a maximum of 1MB of data
            if (typeof data.LastEvaluatedKey != "undefined") {
                console.log("Scanning for more...");
                params.ExclusiveStartKey = data.LastEvaluatedKey;
                docClient.scan(params, onScan);
            }
        }
    }
}